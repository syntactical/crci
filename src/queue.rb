require_relative 'collection'

class MyQueue < Collection
    def add(value)
        new_tail = Node.new(value, nil)

        if (@tail == nil)
            @head.next = new_tail
        end

        @tail = new_tail

        if isEmpty
            @head = @tail
        end
    end

    def remove
        head_in_queue = @head
        @head = @head.next
        
        if @head == nil
            @tail = nil
        end

        head_in_queue
    end

    def peek
        @head
    end

    def isEmpty
        @head == nil
    end
end