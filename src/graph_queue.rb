class GraphQueue
  def initialize
    @store = Array.new
  end
  
  def dequeue
    @store.pop
  end
  
  def enqueue(element)
    @store.unshift(element)
    self
  end
  
  def size
    @store.size
  end

  def isEmpty
    @store.empty?
  end

  def values
    @store.map(&:value)
  end
end