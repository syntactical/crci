require_relative 'graph_queue'

class Graph
    def initialize(nodes)
        @nodes = nodes
    end

    def depth_first_search(root)
        @route = []

        visit_neighbors_depth_first(root)

        reset_graph

        @route
    end

    def breadth_first_search(root)
        @route = []

        queue = GraphQueue.new
        queue.enqueue(root)

        while !queue.isEmpty
            # require 'byebug'
            # byebug
            current_node = queue.dequeue
            current_node.marked = true
            @route << current_node.value
            if current_node.neighbors != nil
                current_node.neighbors.each do |neighbor|
                    queue.enqueue(neighbor) if neighbor.marked == false
                    neighbor.marked = true
                end
            end
        end

        reset_graph

        @route
    end

    def visit_neighbors_depth_first(node)
        node.marked = true
        @route << node.value
        if node.neighbors != nil
            node.neighbors.each do |neighbor|
                visit_neighbors_depth_first(neighbor) if neighbor.marked == false
            end
        end
    end

    def reset_graph
        @nodes.each do |node|
            node.marked = false
        end
    end
end