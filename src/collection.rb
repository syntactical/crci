require_relative 'node'

class Collection
    def initialize(value)
        @head = Node.new(value, nil)
    end

    def to_array
        array = []
        current_node = @head

        array << current_node.value

        while current_node.next != nil
            current_node = current_node.next
            array << current_node.value
        end

        array
    end
end