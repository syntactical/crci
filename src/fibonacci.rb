class Fibonacci
    def initialize()
        @memo = {}
        @memo[0] = 0
        @memo[1] = 1
    end

    def compute(n)
        if(@memo[n] != nil)
            @memo[n]
        else 
            result = compute(n-1) + compute(n-2)
            @memo[n] = result
            result
        end
    end

    def compute_top_down(n)
        top_down(n, [])
    end

    def top_down(n, memo)
        return n if n == 0 || n == 1

        if memo[n] == nil
            memo[n] = top_down(n-1, memo) + top_down(n-2, memo)
        end

        memo[n]
    end

    def compute_bottom_up(n)
        return n if n == 0 || n == 1

        memo = []
        memo[0] = 0
        memo[1] = 1

        (2..n).each do |i|
            memo[i] = memo[i-2] + memo[i-1]
        end

        memo[n]
    end

    def compute_bottom_up_2(n)
        return n if n == 0 || n == 1

        a = 0
        b = 1

        (n-2).times do
            c = a + b
            a = b
            b = c
        end

        a+b
    end
end