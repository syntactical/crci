class ArraySorter
    def bubble_sort(array)
        array.length.times do |i|
            (0..array.length - i - 2).each do |n|
                if array[n] > array[n+1]
                    swap(array, n, n+1)
                end
            end
        end
        array
    end

    def selection_sort(array)
        array.length.times do |i|
            local_min_index = i
            (i..array.length-1).each do |n|
                if array[n] < array[local_min_index]
                    local_min_index = n
                end
            end
            swap(array, i, local_min_index)
        end
        array
    end

    def merge_sort(array)
        merge_sort_recursive(array, [], 0, array.length - 1)
        array
    end

    def merge_sort_recursive(array, helper, low, high)
        if low < high
            middle = (low + high) / 2
            merge_sort_recursive(array, helper, low, middle)
            merge_sort_recursive(array, helper, middle + 1, high)
            merge(array, helper, low, middle, high)
        end
    end

    def merge(array, helper, low, middle, high)
        (low..high).each do |i|
            helper[i] = array[i]
        end

        helper_left = low
        helper_right = middle + 1
        current = low

        while(helper_left <= middle && helper_right <= high)
            if(helper[helper_left] < helper[helper_right])
                array[current] = helper[helper_left]
                helper_left += 1
            else
                array[current] = helper[helper_right]
                helper_right += 1
            end
            current += 1
        end

        remaining = middle - helper_left

        (0..remaining).each do |i|
            array[current + i] = helper[helper_left + i]
        end
    end

    def quick_sort(array)
        quick_sort_recursive(array, 0, array.length - 1)
    end

    def quick_sort_recursive(array, left, right)
        index = partition(array, left, right)
        if left < index - 1
            quick_sort_recursive(array, left, index - 1)
        end
        if right > index
            quick_sort_recursive(array, index, right)
        end
        array
    end

    def partition(array, left, right)
        pivot = array[(left + right) / 2]

        while left <= right
            while array[left] < pivot
                left += 1
            end

            while array[right] > pivot
                right -= 1
            end

            if left <= right
                swap(array, left, right)
                left += 1
                right -= 1 
            end
        end

        left
    end

    def swap(array, index1, index2)
        temp_value_1 = array[index1]
        temp_value_2 = array[index2]

        array[index2] = temp_value_1
        array[index1] = temp_value_2
    end
end
