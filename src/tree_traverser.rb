require_relative 'tree_node'

class TreeTraverser
    def in_order_traversal(node)
        @array = []

        visit_in_order(node)   

        @array
    end

    def post_order_traversal(node)
        @array = []

        visit_post_order(node)   

        @array
    end

    def pre_order_traversal(node)
        @array = []

        visit_pre_order(node)   

        @array
    end

    private

    def visit_in_order(node)
        if node != nil
            visit_in_order(node.left)
            @array << node.value
            visit_in_order(node.right)
        end
    end

    def visit_post_order(node)
        if node != nil
            visit_post_order(node.left)
            visit_post_order(node.right)
            @array << node.value
        end
    end

    def visit_pre_order(node)
        if node != nil
            @array << node.value
            visit_pre_order(node.left)
            visit_pre_order(node.right)
        end
    end
end