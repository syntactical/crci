class MinHeap
    def initialize
        @elements = [nil]
    end

    def add(value)
        @elements << value
        sift_up(@elements.length - 1)
    end

    def pop
        swap(1, @elements.size-1)
        min = @elements.pop
        sift_down(1)
        min
    end

    def sift_up(index)
        if index <= 1
            return
        end

        parent_index = index./2

        if @elements[parent_index] > @elements[index]
            swap(parent_index, index)
            sift_up(parent_index)
        end
    end

    def sift_down(index)
        child_index = index * 2

        return if child_index > @elements.size - 1

        if @elements.size - 1 > child_index && @elements[child_index] > @elements[child_index + 1]
            child_index += 1
        end

        # require 'byebug'
        # byebug

        if @elements[index] > @elements[child_index]
            swap(index, child_index)
            sift_down(child_index)
        end
    end

    def swap(index1, index2)
        index_1_value = @elements[index1]
        index_2_value = @elements[index2]

        @elements[index1] = index_2_value
        @elements[index2] = index_1_value
    end

    def min
        @elements[1]
    end
end