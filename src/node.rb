class Node
    attr_accessor :next
    attr_accessor :value

    def initialize(value, next_node)
        @value = value
        @next = next_node
    end
end
