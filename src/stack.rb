require_relative 'collection'

class Stack < Collection
    def push(value)
        new_head = Node.new(value, nil)
        new_head.next = @head
        @head = new_head
    end

    def pop
        popped_item = @head
        @head = popped_item.next
        popped_item
    end

    def peek
        @head
    end

    def isEmpty
        @head == nil
    end
end