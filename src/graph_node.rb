require_relative 'node'

class GraphNode
    attr_accessor :value, :neighbors, :marked

    def initialize(value, neighbors=nil, marked=false)
        @value = value
        @neighbors = neighbors
        @marked = marked
    end
end