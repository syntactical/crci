require_relative 'collection'

class LinkedList < Collection
    def append(value)
        current_node = @head
        while current_node.next != nil
            current_node = current_node.next
        end

        current_node.next = Node.new(value, nil)
    end

    def delete(value)
        current_node = @head

        if current_node.value == value
            @head = current_node.next
        else
            while current_node.next != nil && current_node.next.value != value
                current_node = current_node.next
            end
            
            unless current_node.next == nil
                current_node.next = current_node.next.next
            end
        end
    end

    def remove_duplicates
        set = []
        previous = nil
        current_node = @head

        while current_node != nil
            if set.include?(current_node.value)
                previous.next = current_node.next
            else
                set << current_node.value
                previous = current_node
            end
            current_node = current_node.next
        end
    end
end
