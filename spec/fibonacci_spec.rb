require "rspec"
require_relative "../src/fibonacci"

describe 'compute' do
    it 'calculates nth in sequence' do
        fib = Fibonacci.new
        expect(fib.compute(0)).to eq(0)
        expect(fib.compute(1)).to eq(1)
        expect(fib.compute(2)).to eq(1)
        expect(fib.compute(3)).to eq(2)
        expect(fib.compute(4)).to eq(3)
        expect(fib.compute(19)).to eq(4181)
    end

    it 'calculates nth in sequence, top down' do
        fib = Fibonacci.new
        expect(fib.compute_top_down(0)).to eq(0)
        expect(fib.compute_top_down(1)).to eq(1)
        expect(fib.compute_top_down(2)).to eq(1)
        expect(fib.compute_top_down(3)).to eq(2)
        expect(fib.compute_top_down(4)).to eq(3)
        expect(fib.compute_top_down(19)).to eq(4181)
    end
    
    it 'calculates nth in sequence, bottom up' do
        fib = Fibonacci.new
        expect(fib.compute_bottom_up(0)).to eq(0)
        expect(fib.compute_bottom_up(1)).to eq(1)
        expect(fib.compute_bottom_up(2)).to eq(1)
        expect(fib.compute_bottom_up(3)).to eq(2)
        expect(fib.compute_bottom_up(4)).to eq(3)
        expect(fib.compute_bottom_up(19)).to eq(4181)
    end

    it 'calculates nth in sequence, bottom up, second way' do
        fib = Fibonacci.new
        expect(fib.compute_bottom_up_2(0)).to eq(0)
        expect(fib.compute_bottom_up_2(1)).to eq(1)
        expect(fib.compute_bottom_up_2(2)).to eq(1)
        expect(fib.compute_bottom_up_2(3)).to eq(2)
        expect(fib.compute_bottom_up_2(4)).to eq(3)
        expect(fib.compute_bottom_up_2(19)).to eq(4181)
    end
end