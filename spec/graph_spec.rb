require "rspec"
require_relative "../src/graph"
require_relative "../src/graph_node"

describe 'Graph' do
    before :each do
        @node0 = GraphNode.new(0)
        node1 = GraphNode.new(1)
        node2 = GraphNode.new(2)
        node3 = GraphNode.new(3)
        node4 = GraphNode.new(4)
        node5 = GraphNode.new(5)

        @node0.neighbors = [node1, node4, node5]
        node1.neighbors = [node3, node4]
        node2.neighbors = [node1]
        node3.neighbors = [node2, node4]

        @graph = Graph.new([@node0, node1, node2, node3, node4, node5])
    end

    it 'searches breadth first' do
        expect(@graph.breadth_first_search(@node0)).to eq([0,1,4,5,3,2])
    end

    it 'searches depth first' do
        expect(@graph.depth_first_search(@node0)).to eq([0,1,3,2,4,5])
    end
end