def map_nodes_to_values(node_array)
    node_array.map(&:value)
end