require "rspec"
require_relative "../src/stack"

describe '#new' do
    it 'initializes with one node with value provided' do
        stack = Stack.new(8)
        expect(stack.to_array).to eq([8])
    end
end

describe '#push' do
    it 'pushes value to top of stack' do
        stack = Stack.new(8)
        stack.push(9)
        expect(stack.to_array).to eq([9,8])
    end
end

describe '#pop' do
    it 'takes value from top of stack' do
        stack = Stack.new(8)
        stack.push(9)
        expect(stack.pop.value).to eq(9)
        expect(stack.to_array).to eq([8])
    end
end

describe 'peek' do
    it 'looks at value on top of stack' do
        stack = Stack.new(8)
        stack.push(9)
        expect(stack.peek.value).to eq(9)
    end
end
