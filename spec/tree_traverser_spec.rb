require "rspec"
require_relative "../src/tree_traverser"

describe '#in_order_traversal' do
    before :all do
        @root = TreeNode.new(10)
        level1Left = TreeNode.new(6)
        level1Right = TreeNode.new(20)
        l1lLeft = TreeNode.new(4)
        l1lRight = TreeNode.new(8)
        l1rLeft = TreeNode.new(30)
        childLeft = TreeNode.new(1)
        childRight = TreeNode.new(3)

        @root.left = level1Left
        @root.right = level1Right

        level1Left.left = l1lLeft
        level1Left.right = l1lRight

        level1Right.left = l1rLeft

        l1lLeft.left = childLeft
        l1lLeft.right = childRight
    end

    it 'traverses tree in order' do
        expect(TreeTraverser.new.in_order_traversal(@root)).to eq([1,4,3,6,8,10,30,20])
    end

    it 'traverses tree in pre-order' do
        expect(TreeTraverser.new.pre_order_traversal(@root)).to eq([10,6,4,1,3,8,20,30])
    end

    it 'traverses tree in post-order' do
        expect(TreeTraverser.new.post_order_traversal(@root)).to eq([1,3,4,8,6,30,20,10])
    end
end
