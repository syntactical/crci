require "rspec"
require_relative "../src/min_heap"

describe 'MinHeap' do
    before :each do
        @heap = MinHeap.new
        @heap.add(7)
        @heap.add(6)
        @heap.add(4)
        @heap.add(10)
        @heap.add(79)
    end

    it 'finds the minimum' do
        expect(@heap.min).to eq(4)
    end

    it 'removes the minimum and sets the new minimum' do
        @heap.pop
        expect(@heap.min).to eq(6)
        @heap.pop
        expect(@heap.min).to eq(7)
    end
end