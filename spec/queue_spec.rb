require "rspec"
require_relative "../src/queue"

describe '#new' do
    it 'initializes with one node with value provided' do
        queue = MyQueue.new(8)
        expect(queue.to_array).to eq([8])
    end
end

describe '#add' do
    it 'pushes value to end of queue' do
        queue = MyQueue.new(8)
        queue.add(9)
        expect(queue.to_array).to eq([8,9])
    end
end

describe '#remove' do
    it 'takes value from front of queue' do
        queue = MyQueue.new(8)
        queue.add(9)
        expect(queue.remove.value).to eq(8)
        expect(queue.to_array).to eq([9])
    end
end

describe 'peek' do
    it 'looks at value on front of queue' do
        queue = MyQueue.new(8)
        queue.add(9)
        expect(queue.peek.value).to eq(8)
    end
end

describe 'isEmpty' do
    it 'returns true when queue is empty' do
        queue = MyQueue.new(8)
        queue.add(9)
        queue.remove
        queue.remove
        expect(queue.isEmpty).to eq(true)
    end
end
