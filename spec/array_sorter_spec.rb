require 'rspec'
require_relative '../src/array_sorter'

describe 'ArraySorter' do
    before :each do
        @array_sorter = ArraySorter.new
        
        random = Random.new

        @unsorted_array = []
        100.times { @unsorted_array << random.rand(100) }

        @sorted_array = @unsorted_array.sort
    end

    it 'does bubble sort' do
        expect(@array_sorter.bubble_sort(@unsorted_array)).to eq(@sorted_array)
    end

    it 'does selection sort' do
        expect(@array_sorter.selection_sort(@unsorted_array)).to eq(@sorted_array)
    end

    it 'does merge sort' do
        expect(@array_sorter.merge_sort(@unsorted_array)).to eq(@sorted_array)
    end

    it 'does quick sort' do
        expect(@array_sorter.quick_sort(@unsorted_array)).to eq(@sorted_array)
    end
end
