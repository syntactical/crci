require "rspec"
require_relative "../src/linked_list"

describe '#new' do
    it 'initializes with one node with value provided' do
        list = LinkedList.new(8)
        expect(list.to_array).to eq([8])
    end
end

describe '#append' do
    it 'appends node with value to tail of list' do
        list = LinkedList.new(8)
        list.append(9)
        expect(list.to_array).to eq([8,9])
        list.append(10)
        expect(list.to_array).to eq([8,9,10])
    end
end

describe '#delete' do
    it 'deletes node with specified value' do
        list = LinkedList.new(8)
        list.append(9)
        list.append(10)
        list.append(11)
        list.delete(9)

        expect(list.to_array).to eq([8,10,11])
    end

    it 'deletes node with specified value with value is heads value' do
        list = LinkedList.new(8)
        list.append(9)
        list.append(10)
        list.delete(8)

        expect(list.to_array).to eq([9,10])
    end

    it 'deletes node with specified value with value is last nodes value' do
        list = LinkedList.new(8)
        list.append(9)
        list.append(10)
        list.delete(10)

        expect(list.to_array).to eq([8,9])
    end

    it 'does not delete if node with value not found' do
        list = LinkedList.new(8)
        list.append(9)
        list.append(10)
        list.delete(11)

        expect(list.to_array).to eq([8,9,10])
    end
end

describe '#remove_duplicates' do
    it 'removes duplicates' do
        list = LinkedList.new(8)
        list.append(9)
        list.append(10)
        list.append(10)
        list.append(11)
        list.remove_duplicates

        expect(list.to_array).to eq([8,9,10,11])
    end
end